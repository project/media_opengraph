<?php

declare(strict_types = 1);

namespace Drupal\media_opengraph\Plugin\media\Source;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;
use Fusonic\OpenGraph\Consumer;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\HttpFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a media source plugin for displaying links as a rich preview.
 *
 * All data should be treated as unsafe and escaped in any templates using this.
 *
 * @MediaSource(
 *   id = "link_preview",
 *   label = @Translation("OpenGraph Link Preview"),
 *   description = @Translation("Use OpenGraph tags to build a rich link preview."),
 *   allowed_field_types = {"string"},
 *   default_thumbnail_filename = "no-thumbnail.png"
 * )
 */
class LinkPreview extends MediaSourceBase {

  /**
   * The logger channel for media.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected ClientInterface $httpClient;

  /**
   * The Open Graph Consumer.
   *
   * @var \Fusonic\OpenGraph\Consumer
   */
  protected Consumer $consumer;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Constructs a new MediaOpenGraph instance.
   *
   * @param mixed[] $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel for media.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Entity Cache.
   */
  public function __construct(array $configuration, string $plugin_id, mixed $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, ConfigFactoryInterface $config_factory, LoggerInterface $logger, ClientInterface $http_client, CacheBackendInterface $cache) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->logger = $logger;
    $this->httpClient = $http_client;
    $this->consumer = new Consumer($http_client, new HttpFactory());
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      'type' => $this->t('Resource type'),
      'title' => $this->t('Page title'),
      'description' => $this->t('Meta Description'),
      'siteName' => $this->t('Site Name'),
      'twitter:card' => $this->t('Twitter Card Type'),
      'twitter:title' => $this->t('Twitter Title'),
      'twitter:description' => $this->t('Twitter Description'),
      'audios' => $this->t('Meta Audio Tag as Array'),
      'videos' => $this->t('Meta Video Tag as Array'),
      'images' => $this->t('Meta Image Tag as Array'),
      'image_uri_any' => $this->t('First Image - Unified source'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $media_url = $this->getSourceFieldValue($media);
    // The URL may be NULL if the source field is empty, in which case just
    // return NULL.
    if (empty($media_url)) {
      return NULL;
    }
    $cid = "link_preview:" . Crypt::hashBase64($media_url);
    if ($cache = $this->cache->get($cid)) {
      $data = $cache->data;
    }
    else {
      $cache_time = match($this->getConfiguration()['storage_time']) {
        'hour' => 3600,
        'day' => 86400,
        'month' => 2419200,
        'unlimited' => Cache::PERMANENT,
        default => 604800
      };
      try {
        $data = get_meta_tags($media_url);
        @$data += (array) $this->consumer->loadUrl($media_url);
        $data['fetch_time'] = time();
        foreach (['audios', 'videos', 'images'] as $type) {
          if (!empty($data[$type])) {
            foreach ($data[$type] as $i => $datum) {
              $data[$type][$i] = (array) $datum;
            }
          }
        }
        $this->cache->set($cid, $data, $cache_time, ["media:{$media->id()}"]);
      }
      catch (\Throwable $e) {
        $this->logger->error('Failed to load metadata for @url. Error was @error', [
          '@url' => $media_url,
          '@error' => $e->getMessage(),
        ]);
        return NULL;
      }
    }

    switch ($attribute_name) {
      case 'default_name':
      case 'title':
        return isset($data['title']) ? html_entity_decode($data['title'], ENT_QUOTES) : (isset($data['twitter:title']) ? html_entity_decode($data['twitter:title'], ENT_QUOTES) : $media_url);

      case 'image_uri_any':
        return $data['twitter:image'] ?? $data['images'][0]['url'] ?? NULL;

      default:
        return isset($data[$attribute_name]) ? html_entity_decode($data[$attribute_name], ENT_QUOTES) : NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)
      ->set('label', $this->t('Link'))
      ->set('description', $this->t('Enter the link here to use to build preview based on opengraph/meta data'));
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldValue(MediaInterface $media) {
    if (!$url = parent::getSourceFieldValue($media)) {
      return NULL;
    }
    if (isset($media->original)) {
      $config = $this->getConfiguration();
      return match ($config['storage_time']) {
        'hour' => $url . "?refresh=" . date('Y-m-d-H'),
        'day' => $url . "?refresh=" . date('Y-m-d'),
        'month' => $url . "?refresh=" . date('Y-m'),
        'unlimited' => $url,
        default => $url . "?refresh=" . date('Y-W'),
      };
    }
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['storage_time'] = [
      '#type' => 'select',
      '#title' => $this->t('Storage Time'),
      '#default_value' => $this->getConfiguration()['storage_time'] ?? 'week',
      '#options' => [
        'hour' => $this->t('1 Hour'),
        'day' => $this->t('1 Day'),
        'week' => $this->t('1 Week'),
        'month' => $this->t('1 Month'),
        'unlimited' => $this->t('Forever'),
      ],
      '#description' => $this->t('How long to store metadata before triggering an update when the media is re-saved.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array|string {
    $config = parent::defaultConfiguration();
    $config['storage_time'] = 'week';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('logger.factory')->get('media'),
      $container->get('http_client'),
      $container->get('cache.entity')
    );
  }

}
