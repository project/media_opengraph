# Media OpenGraph

Media OpenGraph provides a new media source plugin and a default media type
using it.

This media type renders similarly to pasting a link into Twitter or Facebook :
grabs the metatags / open graph tags from an external link and renders a preview
locally. The default media type maps an image metatag, title and description
with some very basic styling. It is expected that most sites would want to
customize the style/theme.

Example of three rendered URLs:
- First with no description tag
- Second with neither image nor description tags
- Third with description, image, and title tags

![Examples](docs/examples.png)

For a full description of the module, visit the
[project page](https://www.drupal.org/project/media_opengraph).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/media_opengraph).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

The module requires Drupal 9 or 10 with the Media module enabled. Media Library
is recommended.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Site builders can customize the mapping of metatags to fields by creating a
new media type or editing the default media type.
![Field Mapping](docs/media_config.png)

Metatags results from fetching of pages is cached in the Drupal backend cache
for 1hr. Any mapped metadata onto fields on media are normally semi-permanent
ie, only changed if you change the source field and resave. Metatags on pages
outside of your site/control are a lot more likely to change than metadata on
files that you've uploaded. By default data is cached for 1 week.

Advanced site builders can use non-documented metatags by manually editing and
importing config.


## Maintainers

- Nick Dickinson-Wilde - [NickDickinsonWilde](https://www.drupal.org/u/nickdickinsonwilde)
